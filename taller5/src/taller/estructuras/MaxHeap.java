package taller.estructuras;

public class MaxHeap<T extends Comparable<T>> extends Heap<T> 
{

	public MaxHeap(int tam)
	{
		super(tam);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void siftUp()
	{
		int k=size;
		while (k > 1 && less(k/2, k))
		{
		exch(k/2, k);
		k = k/2;
		}
	}

	@Override
	public void siftDown() 
	{
		int k=1;
		while (2*k <= size)
		{
			int j = 2*k;
			if (j < size && less(j, j+1)) j++;
			if (!less(k, j)) break;
			exch(k, j);
			k = j;
		}// TODO Auto-generated method stub
		
	}

	
	
}
