package taller.estructuras;

public abstract class Heap<T extends Comparable<T>> implements IHeap
{
	protected int size;
	protected T[] pq;


	public void add(Object elemento)
	{
		pq[++size] = (T) elemento;
		siftUp();
	}

	public Heap(int tam)
	{
		size=0;
		pq = (T[]) new Comparable[tam+1];     
	}
	
	
	public T peek()
	{
		return pq[1];
	}
	@Override
	public T poll() 
	{
		T max = pq[1]; 
		exch(1, size--);
		pq[size+1] = null;
		siftDown(); 
		return max;

	}

	@Override
	public int size()
	{
		return size;
	}

	@Override
	public boolean isEmpty() 
	{
		return size==0;
	}

	@Override
	public  abstract void siftUp() ;


	public abstract void siftDown() ;

	public void exch(int j, int k)
	{
		T temp=pq[	k];
		pq[k]=pq[j];
		pq[j]=temp;
	}
	public boolean less(int j, int k)
	{
		return pq[j].compareTo(pq[k]) < 0;	
	}
	public boolean more(int j, int k)
	{
		return pq[j].compareTo(pq[k]) > 0;	
	}

}
