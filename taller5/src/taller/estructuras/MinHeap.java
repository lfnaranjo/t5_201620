package taller.estructuras;

public class MinHeap<T extends Comparable<T>> extends Heap<T>
{

	public   MinHeap(int tam)
	{
		super(tam);
	}

	@Override
	public void siftUp() 
	{
		int k=size;
		while (k > 1 && more(k/2, k))
		{
			exch(k/2, k);
			k = k/2;
		}
	}

	@Override
	public void siftDown()
	{
		int k=1;
		while (2*k <= size)
		{
			int j = 2*k;
			if (j < size && more(j, j+1)) j++;
			if (!more(k, j)) break;
			exch(k, j);
			k = j;
		}
	}

}
