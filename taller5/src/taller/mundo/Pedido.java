package taller.mundo;

public class Pedido implements Comparable<Pedido>
{

	// ----------------------------------
	// Atributos
	// ----------------------------------
	public final static int PRECIO = 0;
	/**
	 * Constante que define la accion de atender un pedido
	 */
	public final static int CERCANIA = 1;
	
	
	private int Key;
	/**
	 * Precio del pedido
	 */
	private double precio;
	
	/**
	 * Autor del pedido
	 */
	private String autorPedido;
	
	/**
	 * Cercania del pedido
	 */
	private int cercania;
	
	// ----------------------------------
	// Constructor
	// ----------------------------------
	public  Pedido(String autor,double pprecio, int pcercania )
	{
		cercania=pcercania;
		precio=pprecio;
		autorPedido=autor;		
		Key=PRECIO;
	}
	/**
	 * Constructor del pedido
	 * TODO Defina el constructor de la clase
	 */
	
	// ----------------------------------
	// Métodos
	// ----------------------------------
	
	/**
	 * Getter del precio del pedido
	 */
	public double getPrecio()
	{
		return precio;
	}
	
	public int getKey()
	{
		return Key;
	}
	
	public void setKey(int pKey)
	{
		Key=pKey;
	}
	/**
	 * Getter del autor del pedido
	 */
	public String getAutorPedido()
	{
		return autorPedido;
	}
	
	/**
	 * Getter de la cercania del pedido
	 */
	public int getCercania() 
	{
		return cercania;
	}
	
	
	
	@Override
	public int compareTo(Pedido arg0) 
	{
		if(Key==PRECIO && arg0.getKey()==PRECIO)
		{
			if(precio>arg0.getPrecio())
				return 1;
			else if(arg0.getPrecio()>precio)
				return -1;
		}	
		else if(Key==CERCANIA  && arg0.getKey()==CERCANIA)	
		{
			if(cercania>arg0.getCercania())
				return 1;
			else if(arg0.getCercania()>cercania)
				return -1;		
		}
		return 0;
	}
	
	// TODO 
}
