package taller.mundo;

import taller.estructuras.MaxHeap;
import taller.estructuras.MinHeap;


public class Pizzeria 
{	
	// ----------------------------------
	// Constantes
	// ----------------------------------

	/**
	 * Constante que define la accion de recibir un pedido
	 */
	public final static String RECIBIR_PEDIDO = "RECIBIR";
	/**
	 * Constante que define la accion de atender un pedido
	 */
	public final static String ATENDER_PEDIDO = "ATENDER";
	/**
	 * Constante que define la accion de despachar un pedido
	 */
	public final static String DESPACHAR_PEDIDO = "DESPACHAR";
	/**
	 * Constante que define la accion de finalizar la secuencia de acciones
	 */
	public final static String FIN = "FIN";

	// ----------------------------------
	// Atributos
	// ----------------------------------

	/**
	 * Heap que almacena los pedidos recibidos
	 */
	private MaxHeap<Pedido> recibidos;
	// TODO 

	/**
	 * Getter de pedidos recibidos
	 */
	public MaxHeap<Pedido> getRecibidos()
	{
		return recibidos;
	}
	// TODO 
	/** 
	 * Heap de elementos por despachar
	 */
	private MinHeap<Pedido> aDespachar;
	// TODO 
	/**
	 * Getter de elementos por despachar
	 */
	public MinHeap<Pedido> getADespachar()
	{
		return aDespachar;
	}
	// TODO 

	// ----------------------------------
	// Constructor
	// ----------------------------------

	/**
	 * Constructor de la case Pizzeria
	 */
	public Pizzeria()
	{
		aDespachar=new MinHeap<Pedido>(500);
		recibidos=new MaxHeap<Pedido>(500);
		// TODO 
	}

	// ----------------------------------
	// Métodos
	// ----------------------------------

	/**
	 * Agrega un pedido a la cola de prioridad de pedidos recibidos
	 * @param nombreAutor nombre del autor del pedido
	 * @param precio precio del pedido 
	 * @param cercania cercania del autor del pedido 
	 */
	public void agregarPedido(String nombreAutor, double precio, int cercania)
	{
		recibidos.add(new Pedido( nombreAutor,  precio, cercania));
		// TODO 
	}

	// Atender al pedido más importante de la cola

	/**
	 * Retorna el proximo pedido en la cola de prioridad o null si no existe.
	 * @return p El pedido proximo en la cola de prioridad
	 */
	public Pedido atenderPedido()
	{
		Pedido resp= recibidos.poll();
		if(resp!=null)
		{	resp.setKey(Pedido.CERCANIA);
		aDespachar.add(resp);
		}
		return  resp;
	}

	/**
	 * Despacha al pedido proximo a ser despachado. 
	 * @return Pedido proximo pedido a despachar
	 */
	public Pedido despacharPedido()
	{
		Pedido resp= aDespachar.poll();
		return  resp;	
	}

	/**
	 * Retorna la cola de prioridad de pedidos recibidos como un arreglo. 
	 * @return arreglo de pedidos recibidos manteniendo el orden de la cola de prioridad.
	 */
	public Pedido [] pedidosRecibidosList()
	{
		MaxHeap<Pedido>temp=new MaxHeap<Pedido>(500);
		Pedido [] resp=new  Pedido [recibidos.size()];
		int i=0;
		Pedido a=recibidos.poll();
		while(a!=null   )
		{		 
			temp.add(a);
			resp[i++]=a;
			a=recibidos.poll();
		}
		recibidos=temp;
		return resp;
	}

	/**
	 * Retorna la cola de prioridad de despachos como un arreglo. 
	 * @return arreglo de despachos manteniendo el orden de la cola de prioridad.
	 */
	public Pedido [] colaDespachosList()
	{

		MinHeap<Pedido>temp=new MinHeap<Pedido>(500);
		Pedido [] resp=new  Pedido [aDespachar.size()];
		int i=0;
		Pedido a=aDespachar.poll();
		while(a!=null)
		{		 
			temp.add(a);
			resp[i++]=a;
			a=aDespachar.poll();
		}
		aDespachar=temp;
		return resp;
	}
}
